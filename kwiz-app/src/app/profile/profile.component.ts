import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { QuizService } from '../quiz.service';
import { User } from '../_models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  displayName: string = '';
  entries: any[] = [];

  constructor(
    public authService: AuthService,
    private quizService: QuizService
  ) {
    this.displayName = this.authService.getDisplayName() || '';
  }

  ngOnInit(): void {
    this.authService.getAttemptedQuizes().subscribe((doc) => {
      let user = doc.payload.data() as User;
      this.entries = [];
      user.attemptedQuizes.forEach((attemptQuiz) => {
        this.quizService
          .getQuizById(attemptQuiz.category, attemptQuiz.quizId)
          .subscribe((quiz) => {
            this.entries.push({
              quiz: quiz.data(),
              user: attemptQuiz,
            });
          });
      });
    });
  }

  updateDisplayName() {
    this.authService
      .updateDisplayName(this.displayName)
      .then(() => {
        console.log('updated displayName!!');
      })
      .catch((err) => console.log('fail to update displayName', err));
  }

  show() {
    console.log(this.entries);
  }

  setEntryToService(entry: any) {
    this.quizService.setHistoryEntry(entry)
  }
}
