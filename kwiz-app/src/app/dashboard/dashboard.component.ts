import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  navlist: Array<string>;

  constructor(private route: Router,private activatedroute:ActivatedRoute) {
    this.navlist=["JAVA", "PYTHON", "JAVASCRIPT","ANGULAR"];
  }

  ngOnInit(): void {
     this.route.navigateByUrl('home/subject/' + this.navlist[0])
  }

}
